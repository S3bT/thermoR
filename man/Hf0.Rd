% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/gasPure.R
\name{Hf0}
\alias{Hf0}
\title{Return the enthalpy of formation at 0 K of a specie}
\usage{
Hf0(specieName)
}
\arguments{
\item{specieName}{Name of the specie provided as a string}
}
\value{
enthalpy of formation of species at 0 K in kJ/mol
}
\description{
This function returns the enthalpy of formation at 0 K for a specie if
it is in thermData
}
\examples{
CO2 <- Hf0("CO2")
}
\keyword{entropy}
\keyword{entropy,}
\keyword{formation}
\keyword{of}
\keyword{thermData,}
