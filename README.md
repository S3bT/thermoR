
# thermoR

<!-- badges: start -->
<!-- badges: end -->

The goal of thermoR is to enable simple thermodynamic calculations in R. thermoR
uses the Extended Third Millenium Ideal Gas Thermochemical Database

> Elke Goos, Alexander Burcat and Branko Ruscic
> Extended Third Millenium Ideal Gas Thermochemical Database with updates from 
> Active Thermochemical Tables
> <http://burcat.technion.ac.il/dir>;date.
> mirrored at <http://garfield.chem.elte.hu/Burcat/burcat.html>;date.

## Installation

You can install the released version of thermoR from gitLab

``` r
# install.packages("devtools")
devtools::install_git("https://gitlab.com/S3bT/thermoR")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(thermoR)

# set up fuel
speciesVec      <- c("CH4 RRHO", "C2H6", "C3H8", "C4H10 n-butane", "C5H12 n-pentane", "N2 REF", "CO2")
moleFracVec     <- c(0.8334, 0.0989, 0.0294, 0.0073, 0.0023, 0.0085, 0.0202)
fuel <- fuel(speciesVec = speciesVec, moleFracVec = moleFracVec)

# set up humid air 
humidAir <- humidAir(phi = 0.5, temperature_K = 20 + 273.15, pressure_Pa = 101325)

# max temperature
maxTemperature <- adFlameTemp(fuel, humidAir, 1, 15 + 273.15, 15 + 273.15 , 1200 + 273.15)
```

