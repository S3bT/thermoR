#' linear interpolation
#'
#' Function for linear interpolation
#'
#' @keywords linear interpolatin, interpolation
#' @param x parameter to evaluate at
#' @param x1 x-value lower than x
#' @param x2 x-value higher than x
#' @param y1 y-value corresponding to x1
#' @param y2 y-value corresponding to x2
#' @return interpolated y value
#' @export
#' @examples
#'
#' linear_interpolation(x = 5, x1 = 0, x2 = 10, y1 = 50, y2 = 100)
#'
linear_interpolation <- function(x, x1, x2, y1, y2) {
  y1 + (y2 - y1) / (x2 - x1) * (x - x1)
}


#' Fits a dataset to a sutherland model
#'
#' This function fits a stutherland model of the form
#' eta = (A * T^(1/2) / (1 + (T_s/?T))
#' using non linear least squares. The model can directly be used in OpenFOAM
#'
#' @keywords thermData, dry air, sutherland
#' @param dyn_vis_Pas vector of dynamic viscosities in Pa*s
#' @param temperature_K vector of Temperatures in K
#' @param plot weather to plot results (defaults to TRUE)
#' @return a nonlinear least squares fitted model
#' @export
#' @examples
#'
#' data <- data.table::data.table(dynVis = c(1.814858e-05, 1.875865e-05, 1.938272e-05,
#'                               2.002080e-05, 2.067291e-05),
#'                    temp_K  = c(293.15, 305.74, 318.88, 332.59, 346.88))
#'
#' fit_sutherland(data$dynVis, data$temp_K)
fit_sutherland <- function(dyn_vis_Pas, temperature_K, plot = TRUE ){
  if(!  length(dyn_vis_Pas) == length(temperature_K)){
    stop("Length of dyn_vis_Pas (", length(dyn_vis_Pas),
         ") and temperature_K (", length(temperature_K), ") must be equal")
  }
  data <- data.table(eta = dyn_vis_Pas, T = temperature_K)
  model <- nls(eta ~ ((As * (T)^(1 / 2)) / (1 + (Ts / T))),
               data, start = list(As = 1.458e-6, Ts = 110))
  data[, fitted := fitted(model)]
  data[, rel_err := (fitted - eta) / eta]

  if (plot) {
    par(mfrow = c(1, 2))

    plot(data$T, data$eta, panel.first = grid(),
         xlab = "temperature [K]",
         ylab = "dynamic viscosity [Pas]",
         main="sutherland model")
    lines(data$T, data$fitted, col = "red")

    plot(data$T,data$rel_err * 100, panel.first = grid(),
         xlab = "temperature [K]",
         ylab = "relative model error [%]",
         main = "model error")
    abline(h = 0)
  }
  return(model)
}


#' Calculates the central derivative
#'
#' This function calculates the central derivative of a given data set where
#' possible. For the first and last value this is not possible, accordingly
#' for those cases forward and backward differencing is used due to lack of
#' information.
#' @keywords derivative, central derivative
#' @param x vector of x-values
#' @param y vector of y-values
#' @return vector of derivatives
#' @export
#' @examples
#'
#' x <- seq(1, 10, 1)
#' y <- runif(10, 0, 10)
#'
#' d <- central_derivative(x, y)
central_derivative <- function(x, y) {
  if (length(x) != length(y)) {
    stop('x and y vectors must have equal length')
  }
  n <- length(x)
 #  Initialize a vector of length n to enter the derivative approximations
  fdx <- vector(length = n)
  # Initialize first value with forward differencing
  fdx[1] <- (y[2] - y[1]) / (x[2] - x[1])
  # Iterate through the values using the forward differencing method
  for (i in 2:(n-1)) {
    fdx[i] <- (y[i-1] - y[i+1]) / (x[i-1] - x[i+1])
  }
  # Initialize last value with backward differencing
  fdx[n] <- (y[n] - y[n-1]) / (x[n] - x[n-1])
  return(fdx)
}


#' Calculates a polynomial model of supplied order
#'
#' This function calculates a polynomial regression of arbitrary supplied order.
#' If needed, the model may be split into two separate polynomial regressions at
#' the first infliction point of the dataset. This is useful when needing to
#' supply JANAF style polynomes (eg. OpenFOAM). The infliction point is
#' internally calculated using central derivatives. If wanted, the function also
#' shows a plot of the dataset alongside the relative errors.
#'
#' @keywords model, polynome, regression
#' @param x vector of x-values
#' @param y vector of y-values
#' @param split_at_infliction bool weather to split model at infliction point.
#' Defaults to FALSE
#' @param plot bool weather to plot model and relative errors
#' @return list with dataset, model results and derivatives if calculated
#' (data), value of split_at_infliction (split_at_infliction), the model(s)
#' (either model or model1 and model2) and the x-value of the
#' infliction point (ip)
#' @export
#' @examples
#' speciesVec      <- c("CH4 RRHO", "C2H6", "C3H8", "C4H10 n-butane",
#'                      "C5H12 n-pentane", "C6H14,n-hexane", "N2 REF", "CO2")
#' moleFracVec     <- c(0.8476    , 0.033 , 0.0046,  0.00177,
#'                      0.00047, 0.00016, 0.1002, 0.0122)
#'
#' p <- 101800
#' phi <- 0.66
#' TA <- 20 +273.15
#' TF <- 8 + 273.15
#'
#' fuel <- fuel(speciesVec = speciesVec, moleFracVec = moleFracVec)
#' humidAir <- humidAir(phi, TA, p)
#'
#' data <- data.table()
#' data_tmp <- data.table()
#'
#' for (Tad in seq(300, 1200, 50)) {
#'   tmp <- airToFuelRatio(fuel, humidAir, TF, TA, Tad)
#'   data_tmp$Tad     <- round(Tad,2)
#'   data_tmp$Cp      <- CpMixture(speciesVec = tmp$exhaust$composition$specie,
#'                                 moleFracVec = tmp$exhaust$composition$moleFrac,
#'                                 temperature_K = Tad)
#'   data <- rbind(data, data_tmp)
#' }
#'
#' model <- poly_model(x = data$Tad, y = data$Cp, poly_order = 4,
#'                     split_at_infliction = TRUE, plot = TRUE)
#'
poly_model <- function(x, y, poly_order = 4,
                       split_at_infliction = FALSE, plot = TRUE){

  # prelininary safeguards
  ##############################################################################

  if (length(x) != length(y)) {
    stop('x and y vectors must have equal length')
  }

  # MAIN
  ##############################################################################

  # assign data to data.table
  data <- data.table(x=x, y=y)

  if (!split_at_infliction){
    # since no split is wanted assign model of order 'poly_order' and write
    # results to data
    model <- lm(data$y ~ poly(data$x, poly_order, raw = T))
    data[, model := fitted(model)]
  } else {
    # since split at infliction point is wanted, calculate the first and second
    # derivative, since points of infliction occur at d2 = 0. Zero crossings are
    # detected using the signum function. When using diff with signum the number
    # is only not zero if the sign changes (eg. 1 - 1 = 0; 1 - (-1) = -2)
    data[, d := central_derivative(x, y) ]
    data[, d2 := central_derivative(x, d)]
    ip_idx <- data[ c(0, diff(sign(data$d2))) != 0 , , which = TRUE]
    if (length(ip_idx) == 0) {
      # Safeguard against no infliction points
      split_at_infliction <- FALSE
      warning("No point of incliction. Defaulting to not splitting model")
      model <- lm(data$y ~ poly(data$x, poly_order, raw = T))
      data[, model := fitted(model)]
    } else {
      if (length(ip_idx) > 1) {
        # safeguard against more than one infliction point
        ip_idx <- ip_idx[1]
        warning("More than one point of incliction. Only using first one")
      }
      # calculate models for before and after the infliction point
      model1 <- lm(data[1:ip_idx,]$y ~
                     poly(data[1:ip_idx,]$x,poly_order, raw = T))

      model2 <- lm(data[ip_idx:nrow(data)]$y ~
                     poly(data[ip_idx:nrow(data)]$x,poly_order, raw = T))
      # assign results to data. model2 starts at 2 because ip is in model1
      data[, model := c(fitted(model1),
                        fitted(model2)[2:length(fitted(model2))])] #
    }
  }

  # Calculate errors on the model
  data[, err := (y-model)]
  data[, relErr := err/y]

  # plot results
  ##############################################################################
  if (plot) {
    # set up for 2 plots
    par(mfrow=c(1,2))
    # plot data points and model
    plot(data$x,data$y, panel.first = grid(),
         xlab = "x", ylab = "y",
         main=paste(if(split_at_infliction){"split"},
                    "model of order", poly_order))
    lines(data$x,data$model, col="red" )
    # add split line
    if (split_at_infliction) { # add split line
      abline(v = data[ip_idx]$x, col = "blue", lty = 2)
      mtext(paste(round(data[ip_idx]$x,2)), side=1, line=0, at=data[ip_idx]$x, col="blue")
    }
    #plot relative model error
    plot(data$x,data$relErr * 100, panel.first = grid(), xlab = "x", ylab = "relative model error [%]", main = "model error")
    abline(h = 0)
    # add split line
    if (split_at_infliction) {
      abline(v = data[ip_idx]$x, col = "blue", lty = 2)
      mtext(paste(round(data[ip_idx]$x,2)), side=1, line=0, at=data[ip_idx]$x, col="blue")
    }
  }

  # return models
  ##############################################################################
  if (!split_at_infliction) {
    return(list(data = data,
                split_at_infliction = split_at_infliction,
                model = model))
  } else {
    return(list(data=data,
                split_at_infliction = split_at_infliction,
                model1=model1,
                model2=model2,
                ip = data[ip_idx]$x))
  }
}


#' Calculate volumetric flow and pressure increases from fan curves
#'
#' This function calculates the volumetric flowrate and the static pressure
#' increase from provided power and static pressure fan curves. For this
#' function to work power measurments with according fan setting are nessecary
#' alongside several parameters from the fans technical datasheet
#'
#' @param stat_pressure_curve data.frame or data.table containing volumetric
#' flowrate in the first column and corresponding static pressure increase in
#' the second column. These are to be extracted from the fan curves provided in
#' the fans technical data. Units are arbitrary, but must be used consistently
#' with this function if no specific unit is required by the function.
#' @param power_curve data.frame or data.table containing volumetric
#' flowrate in the first column and corresponding power increase in
#' the second column. These are to be extracted from the fan curves provided in
#' the fans technical data. Units are arbitrary, but must be used consistently
#' with this function if no specific unit is required by the function.
#' @param T_ref_K reference temperature in Kelvin (K) from the technical data sheet
#' of the fan.
#' @param rho_ref_kg_m3 reference density in kilogramm per cubic meter (kg/m^3)
#' from the technical data sheet  of the fan.
#' @param fan_setpoint fan rpm setpoint provided in decimal or percent
#' @param pwr_actual power consumption of the fan at 'fan_setpoint'. Unit must
#' match the units provided in 'power_curve'
#' @param T_actual_K actual temperature of the fluid in the inlet provided in
#' Kelvin (K)
#' @param p_inlet_actual_Pa actual static pressure of the fluid in the inlet
#' provided in Pascale (Pa). If unknown defaults to atmospheric pressure at
#' 101325 Pa
#' @param medium_composition_actual data.frame or data.table containing species
#' and molar- or weight fraction of the actual composition of the fluid.
#' Preferably generated with 'thermoR' functions like (eg. humidAir, or exhaust)
#' @param plot weather to plot adjusted fan curves and working point
#' @return list containing the input parameters for later reference :
#' stat_pressure_curve, power_curve, pwr_actual, T_actual_K,
#' p_inlet_actual_Pa and the results V_actual  and delta_p_stat_actual. The two
#' data.tables also include the adjusted values.
#' @export
#' @examples
#' stat_pressure_curve <- data.frame(V1 = c(108.42, 157.34, 206.27,
#'255.19, 304.12, 353.04,
#'401.97, 450.89, 499.82),
#'V2 = c(811.95, 794.52, 763.63,
#'       720.43, 664.57, 596.15,
#'       515.03, 421.31, 314.66))
#'
#'power_curve <- data.frame(V1 = c(108.42, 157.34, 206.27,
#'                                 259.19, 308.12, 357.04,
#'                                 405.97, 454.89, 503.82),
#'                          V2 = c(29.3, 33.56, 38.53,
#'                                 44.16, 49.33, 54.46,
#'                                 59.26, 63.59, 67.17))
#'
#'T_ref_K           <- 250 + 273.15
#'rho_ref_kg_m3     <- 0.675
#'fan_setpoint      <- 0.6
#'pwr_actual        <- 15
#'T_actual_K        <- 183 + 273.15
#'p_inlet_actual_Pa <- 101325
#'medium_composition_actual <- humidAir(phi = 0.5,
#'                                      temperature_K = 20 + 293.15,
#'                                      pressure_Pa = 101325)$composition
#'
#'fan1 <- calc_fan(stat_pressure_curve = stat_pressure_curve,
#'                 power_curve = power_curve,
#'                 T_ref_K = 250 + 273.15,
#'                 rho_ref_kg_m3 = 0.675,
#'                 fan_setpoint = 0.75,
#'                 pwr_actual = 20,
#'                 T_actual_K = 183 + 273.15,
#'                 p_inlet_actual_Pa = 101325,
#'                 medium_composition_actual = humidAir(0.5, 20 + 293.15,
#'                                            p_inlet_actual_Pa)$composition,
#'                 plot = TRUE)
#'
calc_fan <- function(stat_pressure_curve, power_curve,
                     T_ref_K, rho_ref_kg_m3,
                     fan_setpoint,
                     pwr_actual,
                     T_actual_K = 293.15,
                     p_inlet_actual_Pa = 101325,
                     medium_composition_actual = dryAir()$composition,
                     plot = FALSE) {

  # safeguards
  ##############################################################################

  # ensure right data types of data.tables

  if (any(class(stat_pressure_curve) == "data.frame")) {
    # ensure data.table with 2 columns
    stat_pressure_curve <- as.data.table(stat_pressure_curve[, 1:2])
    # assign names for better understanding
    names(stat_pressure_curve) <- c("V", "p")
  } else {
    stop(cat("stat_pressure_curve is type '", class(stat_pressure_curve),
             "', but should be data.frame or data.table"))
  }

  if (any(class(power_curve) == "data.frame")) {
    # ensure data.table with 2 columns
    power_curve <- as.data.table(power_curve[, 1:2])
    # assign names for better understanding
    names(power_curve) <- c("V", "pwr")
  } else {
    stop(cat("power_curve is type '", class(stat_pressure_curve),
             "', but should be data.frame or data.table"))
  }

  # ensure that fan_setpoint is in decimal
  if (!fan_setpoint <= 1) {
    fan_setpoint <- fan_setpoint / 100
  }

  fan_data <- merge(stat_pressure_curve, power_curve, all = TRUE)

  model_p <- lm(p ~ poly(V, 3), data = stat_pressure_curve)
  model_pwr <- lm(pwr ~ poly(V, 3), data = power_curve)

  fan_data[, p := predict(model_p, fan_data)]
  fan_data[, pwr := predict(model_pwr, fan_data)]
  fan_data[, pwr2 := p * V / 3600]
  fan_data[, eta := pwr2 / pwr * 100]

  # MAIN
  ##############################################################################

  # calculate density of medium

  rho <- rhoMixture_ideal(speciesVec = medium_composition_actual$specie,
                          weightFracVec = medium_composition_actual$weightFrac,
                          temperature_K = T_actual_K,
                          pressure_Pa = p_inlet_actual_Pa) / 1000

  # calculate adjustments


  # calculate adjustments for rpm change

  # V2 = V1*(n2/n1) | (n2/n1) = fan_setpoint -> V2 = V1*fan_setpoint
  fan_data[, V_adj_n := V * fan_setpoint]

  # p2 = p1 * (n2/n1)^2 | (n2/n1) = fan_setpoint -> p2 = p1*fan_setpoint^2
  fan_data[, p_adj_n := p * fan_setpoint^2]

  # pwr2 = pwr1 * (n2/n1)^3 | (n2/n1) = fan_setpoint -> pwr2 = pwr1*fan_setpoint
  fan_data[, pwr_adj_n := pwr * fan_setpoint^3]

  # calculate adjustments for rho change

  # V1 = V2 (no change)

  # p2 = p1 * (rho2 / rho1)
  fan_data[, p_adj_n_rho := p_adj_n * (rho / rho_ref_kg_m3)]

  # pwr2 = pwr1 * (rho2 / rho1)
  fan_data[, pwr_adj_n_rho := pwr_adj_n * (rho / rho_ref_kg_m3)]

  # adjusted power added to fluid
  fan_data[, pwr2_adj_n_rho := p_adj_n_rho * V_adj_n / 3600]

  # adjusted efficiency (same overall but different V band)
  fan_data[, eta_adj_n_rho := pwr2_adj_n_rho / pwr_adj_n_rho * 100]


  # interpolation of actual values

  # find values above and below target by finding zero crossing. Zero crossings
  # are detected using the signum function. When using diff with signum the
  # number is only not zero if the sign changes (eg. 1 - 1 = 0; 1 - (-1) = -2)

  pwr_idx_high <- fan_data[c(0, diff(sign(pwr_adj_n_rho - pwr_actual))) != 0, ,
                           which = TRUE]
  pwr_idx_low  <- pwr_idx_high - 1

  if (length(pwr_idx_high) == 0) {
    if (abs(min(pwr_actual - min(fan_data$pwr_adj_n_rho))) <
        abs(min(pwr_actual -  max(fan_data$pwr_adj_n_rho)))) {
      stop(paste0("pwr_actual (", pwr_actual, ") below lowest possible value (",
                  round(min(fan_data$pwr_adj_n_rho), 2), ")"))
    } else {
      stop(paste0("pwr_actual (", pwr_actual, ") above highest possible value (",
                  round(max(fan_data$pwr_adj_n_rho), 2), ")"))
    }
  }

  # interpolate linearly between point to find accurate fit

  V_actual <- linear_interpolation(pwr_actual,
                                   fan_data[pwr_idx_low]$pwr_adj_n_rho,
                                   fan_data[pwr_idx_high]$pwr_adj_n_rho,
                                   fan_data[pwr_idx_low]$V_adj_n,
                                   fan_data[pwr_idx_high]$V_adj_n)

  delta_p_stat_actual <- linear_interpolation(V_actual,
                                              fan_data[pwr_idx_low]$V_adj_n,
                                              fan_data[pwr_idx_high]$V_adj_n,
                                              fan_data[pwr_idx_low]$p_adj_n_rho,
                                              fan_data[pwr_idx_high]$p_adj_n_rho)

  eta_actual <- linear_interpolation(V_actual,
                                     fan_data[pwr_idx_low]$V_adj_n,
                                     fan_data[pwr_idx_high]$V_adj_n,
                                     fan_data[pwr_idx_low]$eta_adj_n,
                                     fan_data[pwr_idx_high]$eta_adj_n)

  if (!plot) {

    # only return values
    return(list(pwr_actual = pwr_actual,
                T_actual_K = T_actual_K,
                p_inlet_actual_Pa = p_inlet_actual_Pa,
                V_actual = round(V_actual, 1),
                delta_p_stat_actual = round(delta_p_stat_actual, 1),
                eta_actual = round(eta_actual, 1)
    )
    )

  } else {

    # plot data

    par(mfrow = c(1, 3))
    # plot static pressure
    plot(fan_data$V, fan_data$p, type = "l",
         xlim = c(0, max(fan_data$V)),
         ylim = c(0, max(fan_data$p)),
         panel.first = grid(), main = "static pressure",
         xlab = "volumetric flowrate", ylab = "static pressure")
    lines(stat_pressure_curve$V_adj_n, stat_pressure_curve$p_adj_n,
          col = "blue")
    lines(fan_data$V_adj_n, fan_data$p_adj_n_rho, col = "red")
    abline(h = delta_p_stat_actual, col = "red", lty = 2)
    abline(v = V_actual, col = "red", lty = 2)
    mtext(paste(round(V_actual, 1)), side = 3, line = 0, at = V_actual,
          col = "red")
    mtext(paste(round(delta_p_stat_actual, 1)),
          side = 4, line = 0, at = delta_p_stat_actual, col = "red")
    legend("bottomright", inset = 0.025, legend = c("orig", "adj"),
           col = c("black", "red"), lty = 1, cex = 0.8)
    # plot power
    plot(fan_data$V, fan_data$pwr, type = "l",
         xlim = c(0, max(fan_data$V)), ylim = c(0, max(fan_data$pwr)),
         panel.first = grid(), main = "power",
         xlab = "volumetric flowrate", ylab = "power")
    lines(fan_data$V_adj_n, fan_data$pwr_adj_n_rho, col = "red")
    abline(h = pwr_actual, col = "red", lty = 2)
    abline(v = V_actual, col = "red", lty = 2)
    mtext(paste(round(V_actual, 1)), side = 3, line = 0,
          at = V_actual, col = "red")
    mtext(paste(round(pwr_actual, 1)), side = 4, line = 0, at = pwr_actual,
          col = "red")
    legend("bottomright", inset = 0.025, legend = c("orig", "adj"),
           col = c("black", "red"), lty = 1, cex = 0.8)
    # plot efficiency
    plot(fan_data$V, fan_data$eta, type = "l",
         xlim = c(0, max(fan_data$V)), ylim = c(0, max(fan_data$eta)),
         panel.first = grid(), main = "efficiency",
         xlab = "volumetric flowrate", ylab = "efficiency")
    lines(fan_data$V_adj_n, fan_data$eta_adj_n, col = "red")
    abline(h = eta_actual, col = "red", lty = 2)
    abline(v = V_actual, col = "red", lty = 2)
    mtext(paste(round(V_actual, 1)), side = 3, line = 0,
          at = V_actual, col = "red")
    mtext(paste(round(eta_actual, 1)), side = 4, line = 0, at = eta_actual,
          col = "red")
    legend("bottomright", inset = 0.025, legend = c("orig", "adj"),
           col = c("black", "red"), lty = 1, cex = 0.8)

    return(list(fan_data = fan_data,
                pwr_actual = pwr_actual,
                T_actual_K = T_actual_K,
                p_inlet_actual_Pa = p_inlet_actual_Pa,
                V_actual = round(V_actual, 1),
                delta_p_stat_actual = round(delta_p_stat_actual, 1),
                eta_actual = round(eta_actual, 1)
    )
    )
  }
}
